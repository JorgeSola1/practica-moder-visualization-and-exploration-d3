



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
d3.json('practica_airbnb.json')
    .then((featureCollection) => {
        console.log(featureCollection.features)
        drawMap(featureCollection);
    });


var tooltip = d3.select("div").append("div")
    .attr("class", "tooltip")
    .style("position", "absolute") //Para obtener la posicion correcta sobre los circulos
    .style("pointer-events", "none") //Para evitar el flicker
    //.style("opacity", 0)
    .style("visibility", "hidden")
    .style("background-color", "white")
    .style("border", "solid")
    .style("border-width", "1px")
    .style("border-radius", "5px");


function drawMap(featureCollection) {
    console.log(featureCollection)
    console.log(featureCollection.features)

    var width = 700;
    var height = 700;

    var height_barchar = 700;
    var width_barchar = 700;
    var marginbottom = 100;
    var margintop = 50;

    var svgBarchart = d3.select('div')
    .append('svg')
    .attr('width', width_barchar)
    .attr('height', height_barchar + marginbottom + margintop)
    .append("g")
    .attr("transform", "translate( 0," + margintop + ")");
    

    var data = featureCollection.features[0].properties.avgbedrooms

    var mainData = featureCollection.features

    var lenData = mainData.length

    var priceInformation = featureCollection.features

    var avgprice_list = []
    
    for (i = 0; i < lenData; i++ ){
        avgprice_list.push(priceInformation[i].properties.avgprice)
    };
    
   
    avgprice_list.forEach(function(item, i)  { if (item === undefined) avgprice_list[i] = 1});
    
    for(i=0; i< avgprice_list.length; i++){
        console.log(avgprice_list[i])
        if(avgprice_list[i] === undefined){
            console.log(true)
        }
    };
    
    var avgprice = avgprice_list.sort(function(a, b){return b-a});
  
        console.log(data)
        var xscale = d3.scaleBand()
        .domain(data.map(function(d) {
            return d.bedrooms;
        }))
        .range([0, width_barchar])
        .padding(0.1);

        var yscale = d3.scaleLinear()
                       .domain([0, d3.max(data, function(d) {
                        return d.total;
                        })])
                       .range([height_barchar, 0]); 
                       
        var xaxis = d3.axisBottom(xscale);

        //Creacion de los rectangulos
        var rect = svgBarchart.selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr("fill", "#93CAAE");
        
        rect.attr('class', (d) => {
            if (d.value > 10) {
                return 'rectwarning';
            }
        });
        
        rect
            .attr("x", function(d) {
                console.log(d)
                return xscale(d.bedrooms);
            })
            .attr('y', d => {
                return yscale(d.total)
            })
            .attr("width", xscale.bandwidth())
            .attr("height", function(d) {
                return height_barchar - yscale(d.total);//Altura real de cada rectangulo.
            })
            .on('mouseover',handleOverBarchart)
            .on('mouseout',handleOutBarchar);
        
        
        //Añadimos el texto correspondiente a cada rectangulo.
        var text = svgBarchart.selectAll('text')
            .data(data)
            .enter()
            .append('text')
            .text(d => d.total)
            .attr("x", function(d) {
                return xscale(d.bedrooms) + xscale.bandwidth() / 2;
            })
            .attr('y', d => {
                return yscale(d.total + 10) 
            
            });
        
        svgBarchart.append("text")
        .attr("transform",
            "translate(" + (width_barchar / 2) + " ," +
            (height_barchar + 60) + ")")
        .style("text-anchor", "middle")
        .style("font-size",20)
        .text("Número de habitaciones");

        svgBarchart.append("g")
            .attr("transform", "translate(0," + height_barchar + ")")
            .call(xaxis)


    var svgMap = d3.select('div')
        .append('svg')
        .attr('width', width)
        .attr('height', height)
        .append('g')

    var center = d3.geoCentroid(featureCollection); //Encontrar la coordenada central del mapa (de la featureCollection)
    //var center_area = d3.geoCentroid(featureCollection.feature[0]); //Encontrar la coordenada central de un area. (de un feature)

    console.log(center)

    //Para transformar geo coordinates[long,lat] en X,Y coordinates.
    var projection = d3.geoMercator()
        .fitSize([width, height], featureCollection) // equivalente a  .fitExtent([[0, 0], [width, height]], featureCollection)
        //.scale(1000)
        //Si quiero centrar el mapa en otro centro que no sea el devuelto por fitSize.
        //.center(center) //centrar el mapa en una long,lat determinada
        //.translate([width / 2, height / 2]) //centrar el mapa en una posicion x,y determinada

    console.log(projection)

    //Para crear paths a partir de una proyección 
    var pathProjection = d3.geoPath().projection(projection);
    //console.log(pathProjection(featureCollection.features[0]))
    var features = featureCollection.features;
    
    var nblegend = 10;
    var widthRect = (width / nblegend) - 2;
    var heightRect = 10;

    var colors = ['#4e79a7', '#f28e2c', '#e15759', '#76b7b2', '#59a14f', '#edc949', '#af7aa1', '#ff9da7', '#9c755f', '#bab0ab']
    var quantizeScale = d3.scaleQuantize()
                        .domain([0,280])
                        .range(colors);   
        
    var linearScale = d3.scaleLinear()
                        .domain([0, nblegend])
                        .range([0, width]);

    var createdPath = svgMap.selectAll('path')
        .data(features)
        .enter()
        .append('path')
        .attr('d', (d) => pathProjection(d))
        .attr("opacity", function(d, i) {
            d.opacity = 1
            return d.opacity
        })
        .attr('stroke','black').attr('stroke-width',0.25)
        .attr("transform", "translate(0," + 30 + ")")

    createdPath.on('mouseover',handleOver)   
    createdPath.on('mouseout',handleOut)
    createdPath.on('click', handleClick)
        //Asignamos un color a cada path a traves de nuestra escala de colores
    features.map(function(d){
        console.log(d.properties.name)
        console.log(d.properties.avgprice)
    })
    createdPath.attr('fill', (d) => quantizeScale(d.properties.avgprice)); //Cambiar d.properties.name por d.properties.avgprice
    //Creacion de una leyenda
    
    var legend_range = []
    for(i=0; i < colors.length; i++){
        legend_range.push(`${i*(280/nblegend)}-${(i+1)*(280/nblegend)}€`)
    }

    console.log(legend_range)

    var legend = svgMap.append("g")
        .selectAll("rect")
        .data(colors)
        .enter()
        .append("rect")
        .attr('x', (d,i) => linearScale(i))
        .attr('width', widthRect)
        .attr('height', heightRect)
        .attr("fill", (d) => d);

    var text_legend = svgMap.append("g")
        .selectAll("text")
        .data(legend_range)
        .enter()
        .append("text")
        .attr("x", (d, i) => linearScale(i)+12) // o (i * (widthRect + 2))
        .attr("y", heightRect * 2.5)
        .text((d) => d)
        .attr("font-size", 12);

    //Captura de eventos Click
    function handleClick(d,i) {   
        d3.selectAll('path').attr('opacity',0.5);
        d3.select(this).attr('stroke','black').attr('stroke-width',3).attr('opacity',1)
        console.log('HOLA')
        var data_click = d.properties.avgbedrooms
        var yscale = d3.scaleLinear()
                       .domain([0, d3.max(data_click, function(d) {
                        return d.total;
                        })])
                       .range([height_barchar, 0]); 

        rect.data(data_click).attr('y', d => {
            return yscale(d.total)
        })
        .attr("height", function(d) {
            return height_barchar - yscale(d.total); //Altura real de cada rectangulo.
        });
        
        text.data(data_click).text(d => d.total).attr('y', d => {return yscale(d.total) - 10
        });

    };

    function handleOver(d){
        d3.select(this).attr('opacity', 0.5)
        
        tooltip.transition()
        .duration(200)
        .style("visibility", "visible")
        // .style("opacity", .9)
        .style("left", (d3.event.pageX + 20) + "px")
        .style("top", (d3.event.pageY - 30) + "px")
        .text(`Neighborhood: ${d.properties.name},
               Price: ${d.properties.avgprice} Eur`)
    
    };

    function handleOverBarchart(d){
        d3.select(this).attr('fill','#FEFA04')
    };

    function handleOutBarchar(d){
        d3.select(this).attr("fill", "#93CAAE")
    };

    function handleOut(d){
        d3.select(this).attr('opacity', 1)
        d3.selectAll('path').attr('opacity',1).attr('stroke','black').attr('stroke-width',0.25);
            console.log(d.properties.name)
        
    }
    };

// Dato curioso: Si quisiesemos unir  dos archivos Geojson
// npm install -g topojson (Primero instalar node.js) => solo para uniones
// topojson spain.json canarias.json -o full_spain.json