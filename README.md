# Práctica moder visualization and exploration d3
Explicación breve del desarrollo de la práctica.

Como se pedía en el enunciado, he dado visualización a número de propiedades que hay en función del número de camas.
En este caso, he creado una gráfica de barras verticales que indican en la parte superior de la cada barra el número de propiedades, mientras que en el eje x aparecen el rango de número de camas.

Por otro lado, he creado un mapa donde  aparecen las distintas barrios de Madrid.
Al poner el cursor encima de una barrio, aparece la información principal de ese barrio, el nombre y el precio medio.
Si se hace click encima de un barrio, este queda destacado visualmente y a su vez, la gráfica de barras verticales se adapata a la información perteneciente a ese determinado barrio.

